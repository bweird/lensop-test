package com.len.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by meng on 2018/4/29.
 */
@Configuration
@PropertySource("classpath:properties\\limit.properties")
public class SysConfig {

    @Value("${userId}")
    private String userId;

    @Value("${limitUser}")
    private String userName;

    @Value("${roleId}")
    private String roleId;

    @Value("${jobId}")
    private String jobId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    @Bean
    public SysConfig getSysConfig(){
        return new SysConfig();
    }
}
