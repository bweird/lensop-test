
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>lenosp开源脚手架</title>
    <meta name="keywords" content="lenosp,lenos,lenosp开源脚手架,lenos脚手架,lenosp脚手架,lenosp开源框架">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <#--<link rel="shortcut icon" href="<%=request.getContextPath()%>/plugin/x-admin/favicon.ico" type="image/x-icon" />-->
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="${re.contextPath}/plugin/x-admin/css/font.css">
    <link rel="stylesheet" href="${re.contextPath}/plugin/x-admin/css/xadmin.css">
    <script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
    <script>
        $(function  () {
            if (/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {
                $('#explain').hide();
            } else {
                $('#explain').show();
            }
        });
    </script>
</head>
<style>
     pre {
        background-color: #f8f8f8;
        border: 1px solid #eee;
        font-size: 13px;
        /*line-height: 19px;*/
        //padding: 2px 1px;
        border-radius: 3px;
    }
</style>
<body class="login-bg">

<div class="login">
    <div class="message">lenos-开源脚手架</div>
    <div id="darkbannerwrap"></div>

    <form method="post" action="/login" class="layui-form">
        <input name="username" placeholder="用户名"value="admin" autocomplete="off"  type="text" lay-verify="username" class="layui-input" >
        <hr class="hr15">
        <input name="password" lay-verify="password" value="123456" placeholder="密码" autocomplete="off"  type="password" class="layui-input">
        <hr class="hr15">
        <div  class="layui-inline">
            <label class="layui-form-label" style="width:40px;padding: 9px 0px;">验证码:</label>
               <div class="layui-input-inline">
                     <input type="text" name="code" style="width:150px;height:35px;" autocomplete="off" lay-verify="code"   class="layui-input">
              </div>
            <div class="layui-input-inline">
                <img src="" id="code">
            </div>

        </div>
        <#--<div>-->
        <#--<label class="layui-form-label" style="width:40px;padding: 9px 0px;">记住我</label>  由于不好验证 目前去掉-->
            <#--<input type="checkbox"   name="rememberMe" lay-skin="switch" lay-filter="switchTest" lay-text="ON|OFF">-->
        <#--</div>-->
        <hr class="hr15">
        <input value="登录" lay-submit lay-filter="login" style="width:100%;" type="submit">
        <hr class="hr20" >
    </form>


</div>

<div id="explain" style="display:none;border:1px solid white;width:20%;height:55%;position: absolute;top:18%;right:10%;background: white;border-radius: 10px;">
    <fieldset class="layui-elem-field layui-field-title site-title">
    </fieldset>
    <ul class="layui-timeline">
        <li class="layui-timeline-item">
            <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
            <div class="layui-timeline-content layui-text">
                <h3 class="layui-timeline-title">开源地址</h3>
                <p>
                    <h4><a target="_blank" href="https://gitee.com/bweird/lenosp">https://gitee.com/bweird/lenosp</a></h4>
                </p>
            </div>
        </li>
        <li class="layui-timeline-item">
            <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
            <div class="layui-timeline-content layui-text">
                <h3 class="layui-timeline-title">启动说明</h3>
                <p>
                <div class="highlight" style="background: #f8f8f8;    border: 1px solid #eee;width: 90%;border-radius: 6px;">
                <pre>git clone https:<span style="font-style: italic;">//gitee.com/bweird/lenosp.git</span>
mvn clean package
mvn package
java <span class="nt">-jar</span> <span class="hljs-built_in">len</span>-web.jar
                    </pre></div>
                </p>

            </div>
        </li>
    </ul>
</div>
    <script>
  var layer;
  $(function  () {
    layui.use(['form','layer'], function(){
      var form = layui.form;
      form.verify({
        username:function(v){
          if(v.trim()==''){
            return "用户名不能为空";
          }
        }
        ,password:function(v){
          if(v.trim()==''){
            return "密码不能为空";
          }
        },code:function(v){
              if(v.trim()==''){
                  return '验证码不能为空';
              }
          }
      });

      form.render();
    });
    layer = layui.layer;
    var msg='${message}';
    if(msg.trim()!=""){
        layer.msg(msg, {icon: 5,anim:6,offset: 't'});
    }
      $("#code").click(function(){
          var url = "/getCode?"+new Date().getTime();
          this.src = url;
      }).click().show();
    $('#code').on('mouseover',function(){
        layer.tips('点击刷新验证码', this,{time:1000});
    });
  })

  if (window != top)
    top.location.href = location.href;
</script>


<!-- 底部结束 -->
</body>
</html>
